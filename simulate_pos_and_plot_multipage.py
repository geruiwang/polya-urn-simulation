import numpy as np
import matplotlib.pyplot as plt
from time import gmtime, strftime
from scipy.stats import binom, wasserstein_distance, logistic, kurtosis
from matplotlib.backends.backend_pdf import PdfPages

def simulate_pos(A=2, B=4, threshold = 1, T = 1000, trial = 100000, epoch = 1 , sec_para=1000000, factor_power = 0, pdf_obj = None, plot_every = 1000):
    '''
    A, B: intial stakes
    threshold: selfish mining threshold
    T: iteration of simulation
    trial: number of trials
    epoch: recompute stakes every epoch
    sec_para: security parameter to reject old history change (set to very large by default)
    factor_power: >=0 for polynomial, =-1 for logistic, =-2 for exponential
    pdf_obj: for multipage pdf default is None
    plot_every: how many iterations to make a plot
    '''
    if factor_power==0:
        factors = np.ones(T)
    elif factor_power==-10:
        ll=logistic(loc = T/2, scale = T/10)
        factors = ll.cdf(np.arange(T)+1)
    elif factor_power==-11:
        factors = (np.exp((np.arange(T)+1)/T)-1)/(np.e-1)
    elif factor_power<0 and factor_power>-2:
        factors = np.ones(T)
        for i in range(1,T):
            factors[i]=factors[i-1]*(-factor_power)
    else:
        factors = (np.arange(T)+1)/T
        factors = factors**factor_power
    factors_sum = np.sum(factors)
    factors = factors*T/factors_sum # normalization
    #tmp = (factors<=1).astype(np.double)
    #factors = factors * tmp + (1-tmp)*1
    factor_names = [r'1',r'$t/T$',r'$(t/T)^2$',r'$(t/T)^3$',r'$(t/T)^4$',r'$(t/T)^5$']
    if factor_power >=0:
        factor_name = factor_names[factor_power]
    else:
        factor_name=''

    # static stake prob.
    a=A*np.ones(trial) # init A's blocks in the chain
    b=B*np.ones(trial) # init B's blocks in the chain
    randomstring = np.random.rand(T,trial)
    side = np.zeros(trial) # init A's side chain
    side_rwd = np.zeros(trial)
    honest = np.zeros(trial) # init B's honest chain
    honest_rwd = np.zeros(trial)
    results_list = []
    results = a/(a+b+honest_rwd) # current stake ratio
    recompute_each_epoch = np.copy(results) # recompute ratio each epoch
    for i in range(T):
        tmp = (randomstring[i] < recompute_each_epoch).astype(np.double) # newly generated leader
        #tmp = (randomstring[i] < A/(A+B)).astype(np.double) # newly generated leader
        side = side + tmp
        side_rwd = side_rwd + tmp*factors[i]
        honest = honest + (1-tmp)
        honest_rwd = honest_rwd + (1-tmp)*factors[i]
        #selfish = (a/(a+b+honest) > threshold).astype(np.double)
        selfish = (recompute_each_epoch > threshold).astype(np.double)

        longer = (side > honest).astype(np.double) # NOT do selfish mining part
        a = a+ side_rwd*(1-selfish)*(longer)
        b = b+ honest_rwd*(1-selfish)*(1-longer)
        side = side*selfish
        side_rwd = side_rwd*selfish
        honest = honest*selfish
        honest_rwd = honest_rwd*selfish

        #override = ( (side > honest)*(honest>0)*(honest<sec_para) ).astype(np.double) # selfish mining part
        override = ( (side == honest+1)*(honest>=1)*(honest<sec_para) ).astype(np.double) # selfish mining part
        adopt = ((side < honest)+(honest>=sec_para) ).astype(np.double)
        assert (override*selfish == override).all()
        assert (adopt*selfish == adopt).all()
        a =  a + side_rwd*override
        side = side * (1-override)
        side_rwd = side_rwd * (1-override)
        honest = honest * (1-override)
        honest_rwd = honest_rwd * (1-override)

        b = b + honest_rwd * adopt
        honest = honest * (1-adopt)
        honest_rwd = honest_rwd * (1-adopt)
        side = side * (1-adopt)
        side_rwd = side_rwd * (1-adopt)
        results = a/(a+b+honest_rwd)
        if (i+1)%plot_every==0:
            #results_list.append((i+1,a/(a+b)))
            results_list.append((i+1,np.copy(results)))
        if sec_para<T and (i+1)%epoch == (-2*sec_para)%epoch: # simulate Ouroboros
            stake_before2k = np.copy(results)
        if (i+1)%epoch == 0:
            if sec_para<T: # simulate Ouroboros
                recompute_each_epoch = np.copy(stake_before2k)
            else:
                recompute_each_epoch = np.copy(results)
    emd = None
    for i,results in results_list:
        surpass_trial = np.sum(results>threshold)
        pp = binom(i,A/(A+B))
        bin_distribution = pp.pmf(np.arange(i+1))
        bin_space = (np.arange(i+1)+A)/(A+B+i)
        width = 0.001
        plt.clf()
        plt.hist(results, bins=np.linspace(0,1,int(1/width)), label="PoS")
        emd = wasserstein_distance(bin_space, results, u_weights=bin_distribution)
        plt.plot(bin_space,bin_distribution*trial,'--', alpha=0.6, label="static stakes")
        #plt.ylim(0,1500)
        ymin, ymax = plt.ylim()
        results_mean = np.mean(results)
        plt.plot([results_mean, results_mean],[0,ymax],'--k', alpha=0.2)
        plt.text(results_mean,ymax,"empirical mean {:.3f}".format(results_mean))
        if threshold<0.5:
            plt.plot([threshold, threshold],[0,ymax],'--r', alpha=0.2)
            if threshold>0:
                plt.text(threshold,ymax*0.8,"over threshold {}%".format(surpass_trial*100/ trial))
        plt.text(0.1,ymax*0.9,"EMD {:.8f}".format(emd))
        #plt.title("{} T, start({},{}), selfish threshold {:.2f}".format(T,A,B,threshold))
        #plt.title("{} T, start({},{}), epoch {}".format(T,A,B,epoch))
        #plt.title("{} T, start({},{}), sec para {}".format(T,A,B,T))
        plt.title("t={}, start({},{}), reward={} (normalized)".format(i,A,B,factor_name))
        #plt.title("{} T, start({},{})".format(T,A,B))
        plt.legend(loc=7)
        plt.xlabel("stakes of first party")
        plt.ylabel("trials out of {}".format(trial))
        filename=strftime("%Y%m%d%H%M%S", gmtime())
        if pdf_obj is None:
            plt.savefig(filename+'.pdf')
        else:
            pdf_obj.savefig()
    return results_list[-1][-1]
def plot_array(y,y1,y2):
    plt.plot(y,'o-')
    for x in range(len(y)):
        plt.text(x, y[x], "{:.3f}".format(y[x]), color="black", fontsize=12)
    plt.plot([len(y),len(y)+1],[y1,y2],'o')
    plt.text(len(y), y1, "L {:.3f}".format(y1), color="black", fontsize=12)
    plt.text(len(y)+1, y2, "E {:.3f}".format(y2), color="black", fontsize=12)
    plt.xlabel("n")
    plt.ylabel("EMD")
r=simulate_pos(1,2, factor_power=-1.001)
#for inita in [1,2,4]:
#    for mul in [1,4]:
#        A=inita*mul
#        B=(inita+1)*mul
#        pp = PdfPages("{},{}.pdf".format(A,B))
#        simulate_pos(A,B, pdf_obj=pp, epoch = 100, sec_para=10, T=15000, plot_every=250)
#        pp.close()
#        print("Done")
#for inita in [1,2,4]:
#    for mul in [1,2,4,8]:
#        A=inita*mul
#        B=(inita+1)*mul
#        pp = PdfPages("{},{}.pdf".format(A,B))
#        simulate_pos(A,B, pdf_obj=pp, epoch = 100, sec_para=10, T=15000, plot_every=250, threshold = 1/3)
#        pp.close()
#        print("Done")
#for inita in [1,2,4]:
#    for mul in [1,2,4,8,16]:
#        _emds = []
#        A=inita*mul
#        B=(inita+1)*mul
#        pp = PdfPages("{},{}.pdf".format(A,B))
#        for pow in range(6):
#            _emds.append(simulate_pos(A,B,factor_power = pow, pdf_obj=pp, epoch = 100, sec_para=10))
#        emd_logistic = simulate_pos(A,B,factor_power = -1, pdf_obj=pp, epoch = 100, sec_para=10)
#        emd_exponential = simulate_pos(A,B,factor_power = -2, pdf_obj=pp, epoch = 100, sec_para=10)
#        pp.close()
#        plt.clf()
#        plot_array(_emds,emd_logistic,emd_exponential)
#        plt.title("start({},{}), reward be $x^n$".format(A,B))
#        plt.savefig("{},{},emd.pdf".format(A,B))
#for inita in [1,2,4]:
#    for mul in [1,2,4,8,16]:
#        _emds = []
#        A=inita*mul
#        B=(inita+1)*mul
#        pp = PdfPages("{},{}.self.pdf".format(A,B))
#        for pow in range(6):
#            _emds.append(simulate_pos(A,B,factor_power = pow, pdf_obj=pp, threshold = 1/3, epoch = 100, sec_para=10))
#        emd_logistic = simulate_pos(A,B,factor_power = -1, pdf_obj=pp, threshold = 1/3, epoch = 100, sec_para=10)
#        emd_exponential = simulate_pos(A,B,factor_power = -2, pdf_obj=pp, threshold = 1/3, epoch = 100, sec_para=10)
#        pp.close()
#        plt.clf()
#        plot_array(_emds,emd_logistic,emd_exponential)
#        plt.title("start({},{}), reward be $x^n$".format(A,B))
#        plt.savefig("{},{},emd.self.pdf".format(A,B))
#simulate_pos(4,8, T=10)
#print(_emds)
