import json
import numpy as np
from numpy.polynomial.polynomial import polyval
import matplotlib.pyplot as plt
from time import gmtime, strftime
from scipy.stats import binom, wasserstein_distance, logistic, kurtosis
from matplotlib.backends.backend_pdf import PdfPages

def simulate_pos(A=2, B=4, threshold = 1, T = 1000, trial = 100000, epoch = 1 , sec_para=1000000, coefficients = [1], pdf_obj = None, plot_every = None):
    '''
    A, B: intial stakes
    threshold: selfish mining threshold
    T: iteration of simulation
    trial: number of trials
    epoch: recompute stakes every epoch
    sec_para: security parameter to reject old history change (set to very large by default)
    coefficients: for polynomial function of reward
    pdf_obj: for multipage pdf default is None
    plot_every: how many iterations to make a plot
    '''
    factors = polyval((np.arange(T)+1)/T, coefficients)
    if (factors<0).any():
        return None
    factors_sum = np.sum(factors)
    factors = factors*T/factors_sum # normalization
    #tmp = (factors<=1).astype(np.double)
    #factors = factors * tmp + (1-tmp)*
    factor_name=str(coefficients)

    # static stake prob.
    a=A*np.ones(trial) # init A's blocks in the chain
    b=B*np.ones(trial) # init B's blocks in the chain
    randomstring = np.random.rand(T,trial)
    side = np.zeros(trial) # init A's side chain
    side_rwd = np.zeros(trial)
    honest = np.zeros(trial) # init B's honest chain
    honest_rwd = np.zeros(trial)
    results_list = []
    results = a/(a+b+honest_rwd) # current stake ratio
    recompute_each_epoch = np.copy(results) # recompute ratio each epoch
    for i in range(T):
        tmp = (randomstring[i] < recompute_each_epoch).astype(np.double) # newly generated leader
        #tmp = (randomstring[i] < A/(A+B)).astype(np.double) # newly generated leader
        side = side + tmp
        side_rwd = side_rwd + tmp*factors[i]
        honest = honest + (1-tmp)
        honest_rwd = honest_rwd + (1-tmp)*factors[i]
        #selfish = (a/(a+b+honest) > threshold).astype(np.double)
        selfish = (recompute_each_epoch > threshold).astype(np.double)

        longer = (side > honest).astype(np.double) # NOT do selfish mining part
        a = a+ side_rwd*(1-selfish)*(longer)
        b = b+ honest_rwd*(1-selfish)*(1-longer)
        side = side*selfish
        side_rwd = side_rwd*selfish
        honest = honest*selfish
        honest_rwd = honest_rwd*selfish

        #override = ( (side > honest)*(honest>0)*(honest<sec_para) ).astype(np.double) # selfish mining part
        override = ( (side == honest+1)*(honest>=1)*(honest<sec_para) ).astype(np.double) # selfish mining part
        adopt = ((side < honest)+(honest>=sec_para) ).astype(np.double)
        assert (override*selfish == override).all()
        assert (adopt*selfish == adopt).all()
        a =  a + side_rwd*override
        side = side * (1-override)
        side_rwd = side_rwd * (1-override)
        honest = honest * (1-override)
        honest_rwd = honest_rwd * (1-override)

        b = b + honest_rwd * adopt
        honest = honest * (1-adopt)
        honest_rwd = honest_rwd * (1-adopt)
        side = side * (1-adopt)
        side_rwd = side_rwd * (1-adopt)
        results = a/(a+b+honest_rwd)
        if plot_every is not None and (i+1)%plot_every==0:
            #results_list.append((i+1,a/(a+b)))
            results_list.append((i+1,np.copy(results)))
        if sec_para<T and (i+1)%epoch == (-2*sec_para)%epoch: # simulate Ouroboros
            stake_before2k = np.copy(results)
        if (i+1)%epoch == 0:
            if sec_para<T: # simulate Ouroboros
                recompute_each_epoch = np.copy(stake_before2k)
            else:
                recompute_each_epoch = np.copy(results)
    if plot_every is None:
        results_list.append((T,np.copy(results)))
    results = results_list[-1][-1]
    emd = np.sum(np.abs(results-A/(A+B)))/trial
    square_error = np.sum((results-A/(A+B))*(results-A/(A+B)))/trial
    epss=[0.1,0.01,0.001]
    deltas=[]
    for eps in epss:
        deltas.append(np.sum(np.abs(results-A/(A+B))>eps)/trial)
    for eps in epss:
        deltas.append(np.sum(np.abs(results-A/(A+B))>A/(A+B)*eps)/trial)
    return {"emd":emd, "se": square_error,"eps":epss, "delta":deltas,  "function":factor_name}#"result":results,
def point2poly(c3, inflect, extreme_away, c0):# since it will be normalized, c3 can pick 1 or -1, and change c0. Also extreme_away only matters with abs value. Notice it may reach negative, so keep alert!
    assert c3!=0
    c2=inflect*(-3*c3)
    delta = np.real(extreme_away*extreme_away*(36*c3*c3))
    c1=(4*c2*c2-delta)/(12*c3)
    return [c0,c1,c2,c3]
A=1
B=2
total_results = {}
for c3 in [-1,1]:
    for c2 in np.arange(-2,3.1,0.1):
        for c1 in np.arange(0,5.1,0.1):
            for c0 in np.arange(0,5.5,0.5):
                c=point2poly(c3,c2,c1,c0)
                d=simulate_pos(A,B,coefficients=c)
                if d is not None:
                    key=d["function"]
                    del d["function"]
                    total_results[key] = d
with open('{},{},honest,T1000.json'.format(A,B),'w',encoding='utf-8') as fout:
    json.dump(total_results,fout)
#d=simulate_pos(1,2, T=30, coefficients=[0,1])
#d
