import numpy as np
import matplotlib.pyplot as plt
from time import gmtime, strftime

A=4
B=8

trail = 100000
iter = 100
a=A*np.ones(trail)
b=B*np.ones(trail)
randomstring = np.random.rand(iter,trail)
#factors = np.log(np.arange(iter)+2)/10
#factors = np.exp(np.arange(iter)/10)/10
factors = np.sqrt(np.arange(iter)+1)/100
#factors = (np.arange(iter)+1)/10000
tmp = (factors<=1).astype(np.double)
factors = factors * tmp + (1-tmp)*1
factors = np.ones(iter)
#print(randomstring)
for i in range(iter):
    tmp = (randomstring[i] < a/(a+b)).astype(np.double)
    a = a+tmp*factors[i]
    b = b+(1-tmp)*factors[i]
#results.append(a/(a+b))
results = a/(a+b)
#print(results)
#np.histogram(results, bins = np.arange(0,1,20))
width = 0.001
plt.clf()
plt.hist(results, bins=np.linspace(0,1,int(1/width)))  # arguments are passed to np.histogram
plt.title("{} trails, {} iter, start({},{}), factor 1".format(trail,iter,A,B))
filename=strftime("%Y%m%d%H%M%S", gmtime())
plt.savefig(filename+'.pdf')
