import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
A=20
B=40


trail = 100000
iter = 10
width = 0.01
epoch = 100
results = []
a=A*np.ones(trail)
b=B*np.ones(trail)
randomstring = np.random.rand(iter,epoch,trail)
#print(randomstring)
pp = PdfPages('2040_epoch10.pdf')#.format(epoch))
factor = 1
for i in range(iter):
    tmp = (randomstring[i] < a/(a+b)).astype(np.double)
    tmp = np.sum(tmp,axis=0)
    #print(tmp.shape)
    a = a+tmp#*factor
    b = b+(epoch-tmp)#*factor
    #factor = factor *(1+1/(A+B))
    results = a/(a+b)
    plt.clf()
    plt.hist(results, bins=np.arange(0,1+width,width))  # arguments are passed to np.histogram
    plt.ylim(0, trail)
    pp.savefig()
#print(results)
#hist, bin_edges = np.histogram(results, bins = np.arange(0,1+width,width))
#hist.sum()

#plt.title("{} trails".format(trail))
#pp.savefig()
pp.close()
