import numpy as np
import matplotlib.pyplot as plt
from time import gmtime, strftime
from scipy.stats import binom, wasserstein_distance, logistic, beta
from matplotlib.backends.backend_pdf import PdfPages
k=((6+20)/6)**(1/3) -1
r=6*k
tmp=r
res=[]
for i in range(3):
    res.append(tmp)
    tmp=tmp*(1+k)

#res.append(tmp)
print(res)
a=np.ones(3)
for i in range(1,3):
    a[i]=a[i-1]*(1+k)
print(a*20/(np.sum(a)))
#B=beta(1000,2000)
#x=B.pdf(np.linspace(0,1,1000))
#plt.plot(np.linspace(0,1,1000),x)
#plt.show()
#iter=1000
#ll=logistic(loc = iter/2, scale = iter/10)
#ll.cdf(1000)
#factors = ll.cdf(np.arange(iter)+1)
#plt.plot(factors)
#plt.show()
#trail=100000
#pp = binom(trail,1/3)
#bin_distribution = pp.pmf(np.arange(trail+1))
#A=1
#B=2
#bin_space = (np.arange(trail+1)+A)/(A+B+trail)
#np.sum(bin_distribution)
#np.max(bin_distribution)*trail
