import numpy as np
import matplotlib.pyplot as plt
from time import gmtime, strftime
from scipy.stats import binom, wasserstein_distance
from matplotlib.backends.backend_pdf import PdfPages

def simulate_pos(A=2, B=4, threshold = 1, iter = 1000, trail = 100000, epoch = 1 , sec_para=1000, factor_power = 0, start_reward = 0, pdf_obj = None):
    '''
    A, B: intial stakes
    threshold: selfish mining threshold
    iter: iteration of simulation
    trail: number of trails
    epoch: recompute stakes every epoch
    sec_para: security parameter to reject old history change (set to very large by default)
    '''
    factors = ((np.arange(iter)+1)/iter)**factor_power*(1-start_reward) + start_reward
    #tmp = (factors<=1).astype(np.double)
    #factors = factors * tmp + (1-tmp)*1
    factor_name = r"$\frac{t^"+str(factor_power)+r"}{iter^"+str(factor_power)+r"}(1-"+str(start_reward)+")+"+str(start_reward)+r"$"
    # static stake prob.
    a=A*np.ones(trail) # init A's blocks in the chain
    b=B*np.ones(trail) # init B's blocks in the chain
    randomstring = np.random.rand(iter,trail)
    side = np.zeros(trail) # init A's side chain
    honest = np.zeros(trail) # init B's honest chain

    results = a/(a+b+honest) # current stake ratio
    for i in range(iter):
        tmp = (randomstring[i] < A/(A+B)).astype(np.double) # newly generated leader
        side = side + tmp*factors[i]
        honest = honest + (1-tmp)*factors[i]
        selfish = (a/(a+b+honest) > threshold).astype(np.double)

        longer = (side > honest).astype(np.double) # NOT do selfish mining part
        a = a+ side*(1-selfish)*(longer)
        b = b+ honest*(1-selfish)*(1-longer)
        side = side*selfish
        honest = honest*selfish
        #TODO check this
        override = ( (side > honest)*(honest>0)*(honest<sec_para) ).astype(np.double) # selfish mining part
        adopt = ((side < honest)+(honest>=sec_para) ).astype(np.double)
        assert (override*selfish == override).all()
        assert (adopt*selfish == adopt).all()
        a =  a + (honest+1)*override
        side = side - (honest+1)*override
        honest = honest * (1-override)

        b = b + honest * adopt
        honest = honest * (1-adopt)
        side = side * (1-adopt)
        results = a/(a+b+honest)
    static_results = np.copy(results)

    # dynamic stake prob.
    a=A*np.ones(trail) # init A's blocks in the chain
    b=B*np.ones(trail) # init B's blocks in the chain
    randomstring = np.random.rand(iter,trail)
    side = np.zeros(trail) # init A's side chain
    honest = np.zeros(trail) # init B's honest chain

    results = a/(a+b+honest) # current stake ratio
    recompute_each_epoch = np.copy(results) # recompute ratio each epoch
    for i in range(iter):
        tmp = (randomstring[i] < recompute_each_epoch).astype(np.double) # newly generated leader
        side = side + tmp*factors[i]
        honest = honest + (1-tmp)*factors[i]
        selfish = (a/(a+b+honest) > threshold).astype(np.double)

        longer = (side > honest).astype(np.double) # NOT do selfish mining part
        a = a+ side*(1-selfish)*(longer)
        b = b+ honest*(1-selfish)*(1-longer)
        side = side*selfish
        honest = honest*selfish

        override = ( (side > honest)*(honest>0)*(honest<sec_para) ).astype(np.double) # selfish mining part
        adopt = ((side < honest)+(honest>=sec_para) ).astype(np.double)
        assert (override*selfish == override).all()
        assert (adopt*selfish == adopt).all()
        a =  a + (honest+1)*override
        side = side - (honest+1)*override
        honest = honest * (1-override)

        b = b + honest * adopt
        honest = honest * (1-adopt)
        side = side * (1-adopt)
        results = a/(a+b+honest)
        if (i+1)%epoch == 0:
            recompute_each_epoch = np.copy(results)
    #results = a/(a+b+honest)
    surpass_trail = np.sum(results>threshold)

    width = 0.001
    plt.clf()
    plt.hist(results, bins=np.linspace(0,1,int(1/width)), label="PoS")
    plt.hist(static_results, bins=np.linspace(0,1,int(1/width)),alpha=0.6, label="static stakes")
    emd = wasserstein_distance(static_results,results)
    #plt.plot(bin_space,bin_distribution*trail,'--', alpha=0.2)
    ymin, ymax = plt.ylim()
    #plt.plot([threshold, threshold],[0,ymax],'--k', alpha=0.2)
    #plt.text(0.5,ymax*0.9,"{}/{} trails".format(surpass_trail, trail))
    plt.text(0.5,ymax*0.9,"EMD {:.8f}".format(emd))
    #plt.title("{} iter, start({},{}), selfish threshold {:.2f}".format(iter,A,B,threshold))
    #plt.title("{} iter, start({},{}), epoch {}".format(iter,A,B,epoch))
    #plt.title("{} iter, start({},{}), sec para {}".format(iter,A,B,iter))
    plt.title("{} iter, start({},{}), reward={}".format(iter,A,B,factor_name))
    #plt.title("{} iter, start({},{})".format(iter,A,B))
    plt.legend(loc=4)
    plt.xlabel("stakes of first party")
    plt.ylabel("trails out of {}".format(trail))
    filename=strftime("%Y%m%d%H%M%S", gmtime())
    if pdf_obj is None:
        plt.savefig(filename+'.pdf')
    else:
        pdf_obj.savefig()
    return emd
def plot_array(y):
    plt.plot(y,'o-')
    for x in range(len(y)):
        plt.text(x, y[x], "{:.3f}".format(y[x]), color="black", fontsize=12)
    plt.xlabel("n")
    plt.ylabel("EMD")
for mul in [1,2,4,8,16,32]:
    _emds = []
    A=1*mul
    B=2*mul
    pp = PdfPages("{},{}.pdf".format(A,B))
    for pow in range(6):
        _emds.append(simulate_pos(A,B,factor_power = pow, start_reward = 0.2, pdf_obj=pp))
    pp.close()
    plt.clf()
    plot_array(_emds)
    plt.title("start({},{}), reward be $x^n$".format(A,B))
    plt.savefig("{},{},emd.pdf".format(A,B))
#simulate_pos(4,8, iter=10)
#print(_emds)
