import numpy as np
from scipy.stats import binom
for i in range(500,2500,500):
    pp = binom(i,1/3)
    bin_distribution = pp.pmf(np.arange(i+1))
    print(np.sum(bin_distribution))
    print(np.argmax(bin_distribution))
    print(np.max(bin_distribution))
    print('-')

