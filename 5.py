import numpy as np
import matplotlib.pyplot as plt
from time import gmtime, strftime

def simulate_pos(A=2, B=4, threshold = 1, iter = 1000, trail = 100000 ):
    a=A*np.ones(trail)
    b=B*np.ones(trail)
    randomstring = np.random.rand(iter,trail)
    #factors = np.log(np.arange(iter)+2)/10
    #factors = np.exp(np.arange(iter)/10)/10
    #factors = np.sqrt(np.arange(iter)+1)/100
    #factors = np.ones(iter)
    #factors = (np.arange(iter)+1)/10000
    #tmp = (factors<=1).astype(np.double)
    #factors = factors * tmp + (1-tmp)*1
    #print(randomstring)
    side = np.zeros(trail)
    honest = np.zeros(trail)

    results = a/(a+b+honest)
    for i in range(iter):
        tmp = (randomstring[i] < A/(A+B)).astype(np.double)
        side = side + tmp#*factors[i]
        honest = honest + (1-tmp)#*factors[i]
        selfish = (a/(a+b+honest) > threshold).astype(np.double)

        longer = (side > honest).astype(np.double)
        a = a+ side*(1-selfish)*(longer)
        b = b+ honest*(1-selfish)*(1-longer)
        side = side*selfish
        honest = honest*selfish
        #TODO check this
        override = (side > honest).astype(np.double)*(honest>0).astype(np.double)
        adopt = (side < honest ).astype(np.double)
        a =  a + (honest+1)*override
        side = side - (honest+1)*override
        honest = honest * (1-override)

        b = b + honest * adopt
        honest = honest * (1-adopt)
        side = side * (1-adopt)
        results = a/(a+b+honest)
    #results = a/(a+b+honest)
    #print(results)
    #np.histogram(results, bins = np.arange(0,1,20))
    surpass_trail = np.sum(results>threshold)
    width = 0.001
    plt.clf()
    plt.hist(results, bins=np.linspace(0,1,int(1/width)))  # arguments are passed to np.histogram
    ymin, ymax = plt.ylim()
    plt.plot([threshold, threshold],[0,ymax],'--k', alpha=0.2)
    plt.text(0.5,ymax-200,"{}/{} trails".format(surpass_trail, trail))
    plt.title("{} iter, start({},{}), selfish threshold {:.2f}".format(iter,A,B,threshold))
    filename=strftime("%Y%m%d%H%M%S", gmtime())
    plt.savefig(filename+'.pdf')

for mul in [1,2,4,8,16,32,64,128,256,512]:
    simulate_pos(1*mul,9*mul, 1/3)
