import numpy as np
#from scipy.special import btdtr, beta
from scipy.misc import comb
from scipy.stats import beta
import matplotlib.pyplot as plt
A=1
B=2
x = np.linspace(0, 1,1000)
for ten in [1,10,100,1000]:
    plt.plot(x, beta.pdf(x, A*ten, B*ten), '-',  label=str(ten))
plt.legend()
plt.show()
#comb(2,1)* beta(2,2)/beta(1,1)
eps=0.001
#c1=btdtr(1,200,1/201-eps)
#c2=btdtr(1,200,1/201+eps)
#print(c1)
#print(c2-c1)
for i in [1,10,100,1000,10000]:
    a=A*i
    b=B*i
    c1=btdtr(a,b,a/(a+b)-eps)
    c2=btdtr(a,b,a/(a+b)+eps)
    print(c2-c1)

Bei = 30000
#np.power(1+1/Bei,10000)
