import numpy as np
import matplotlib.pyplot as plt
from time import gmtime, strftime
from scipy.stats import binom, wasserstein_distance, logistic
from matplotlib.backends.backend_pdf import PdfPages

def simulate_pos(A=2, B=4, threshold = 1, iter = 1000, trial = 100000, epoch = 100 , sec_para=10, factor_power = 0, pdf_obj = None, plot_every = 1000):
    '''
    A, B: intial stakes
    threshold: selfish mining threshold
    iter: iteration of simulation
    trial: number of trials
    epoch: recompute stakes every epoch
    sec_para: security parameter to reject old history change (set to very large by default)
    '''
    #ll=logistic(loc = iter/2, scale = iter/10)
    #factors = ll.cdf(np.arange(iter)+1)
    #factors = (np.exp((np.arange(iter)+1)/1000)-1)/(np.e-1)
    if factor_power==0:
        factors = np.ones(iter)
    else:
        factors = (np.arange(iter)+1)/iter
        factors = factors**factor_power
        factors_sum = np.sum(factors)
        factors = factors*iter/factors_sum # normalization
    #tmp = (factors<=1).astype(np.double)
    #factors = factors * tmp + (1-tmp)*1
    factor_names = [r'1',r'$t/iter$',r'$(t/iter)^2$',r'$(t/iter)^3$',r'$(t/iter)^4$',r'$(t/iter)^5$']
    factor_name = factor_names[factor_power]
    # static stake prob.
    a=A*np.ones(trial) # init A's blocks in the chain
    b=B*np.ones(trial) # init B's blocks in the chain
    randomstring = np.random.rand(iter,trial)
    side = np.zeros(trial) # init A's side chain
    honest = np.zeros(trial) # init B's honest chain
    results_list = []
    results = a/(a+b+honest) # current stake ratio
    recompute_each_epoch = np.copy(results) # recompute ratio each epoch
    for i in range(iter):
        tmp = (randomstring[i] < recompute_each_epoch).astype(np.double) # newly generated leader
        #tmp = (randomstring[i] < A/(A+B)).astype(np.double) # newly generated leader
        side = side + tmp*factors[i]
        honest = honest + (1-tmp)*factors[i]
        selfish = (recompute_each_epoch > threshold).astype(np.double)

        longer = (side > honest).astype(np.double) # NOT do selfish mining part
        a = a+ side*(1-selfish)*(longer)
        b = b+ honest*(1-selfish)*(1-longer)
        side = side*selfish
        honest = honest*selfish

        #override = ( (side > honest)*(honest>0)*(honest<sec_para) ).astype(np.double) # selfish mining part
        override = ( (side == honest+1)*(honest>=1)*(honest<sec_para) ).astype(np.double) # selfish mining part
        adopt = ((side < honest)+(honest>=sec_para) ).astype(np.double)
        assert (override*selfish == override).all()
        assert (adopt*selfish == adopt).all()
        a =  a + (honest+1)*override
        side = side - (honest+1)*override
        honest = honest * (1-override)

        b = b + honest * adopt
        honest = honest * (1-adopt)
        side = side * (1-adopt)
        results = a/(a+b+honest)
        if (i+1)%plot_every==0:
            #longer = (side > honest).astype(np.double)
            #_a = a+ side*(longer)
            #_b = b+ honest*(1-longer)
            #results_list.append((i+1,a/(a+b)))
            results_list.append((i+1,np.copy(results)))
        if (i+1)%epoch == (-2*sec_para)%epoch:
            stake_before2k = np.copy(results)
        if (i+1)%epoch == 0:
            recompute_each_epoch = np.copy(stake_before2k)
    emd = None
    print(np.max(side))
    for i,results in results_list:
        surpass_trial = np.sum(results>threshold)
        #pp = binom(i,A/(A+B))
        #bin_distribution = pp.pmf(np.arange(i+1))
        #bin_space = (np.arange(i+1)+A)/(A+B+i)
        width = 0.001
        plt.clf()
        plt.hist(results, bins=np.linspace(0,1,int(1/width)), label="PoS")
        #emd = wasserstein_distance(bin_space, results, u_weights=bin_distribution)
        #plt.plot(bin_space,bin_distribution*trial,'--', alpha=0.6, label="static stakes")
        plt.ylim(0,5000)
        ymin, ymax = plt.ylim()
        results_mean = np.mean(results)
        plt.plot([results_mean, results_mean],[0,ymax],'--k', alpha=0.2)
        plt.text(results_mean,ymax,"empirical mean {:.3f}".format(results_mean))
        if threshold<0.5:
            plt.plot([threshold, threshold],[0,ymax],'--r', alpha=0.2)
            if threshold>0:
                plt.text(0.5,ymax*0.8,"{}/{} trials".format(surpass_trial, trial))
        #plt.text(0.5,ymax*0.9,"EMD {:.8f}".format(emd))
        #plt.title("{} iter, start({},{}), selfish threshold {:.2f}".format(iter,A,B,threshold))
        #plt.title("{} iter, start({},{}), epoch {}".format(iter,A,B,epoch))
        #plt.title("{} iter, start({},{}), sec para {}".format(iter,A,B,iter))
        plt.title("{} iter, start({},{}), reward={} (normalized)".format(i,A,B,factor_name))
        #plt.title("{} iter, start({},{})".format(iter,A,B))
        plt.legend(loc=7)
        plt.xlabel("stakes of first party")
        plt.ylabel("trials out of {}".format(trial))
        filename=strftime("%Y%m%d%H%M%S", gmtime())
        if pdf_obj is None:
            plt.savefig(filename+'.pdf')
        else:
            pdf_obj.savefig()
    return emd
def plot_array(y):
    plt.plot(y,'o-')
    for x in range(len(y)):
        plt.text(x, y[x], "{:.3f}".format(y[x]), color="black", fontsize=12)
    plt.xlabel("n")
    plt.ylabel("EMD")
print("Start")
#for inita in [1,2,4]:
#    for mul in [1,2,4]:
#        A=inita*mul
#        B=(inita+1)*mul
#        pp = PdfPages("{},{}.pdf".format(A,B))
#        simulate_pos(A,B, threshold = 1/3, iter=15000, plot_every=250, pdf_obj=pp)
#        pp.close()
#        print("Done")
simulate_pos(1,2,threshold = 1)
#for mul in [1,2,4,8,16,32]:
#    _emds = []
#    A=1*mul
#    B=2*mul
#    pp = PdfPages("{},{}.pdf".format(A,B))
#    for pow in range(6):
#        _emds.append(simulate_pos(A,B,factor_power = pow, pdf_obj=pp, threshold = 1/3))
#    pp.close()
#    plt.clf()
#    plot_array(_emds)
#    plt.title("start({},{}), reward be $x^n$".format(A,B))
#    plt.savefig("{},{},emd.pdf".format(A,B))
#simulate_pos(4,8, iter=10)
#print(_emds)
